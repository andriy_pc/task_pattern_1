package bakery;

import units.pizza.Pizza;
import units.pizza.PizzaModel;

public abstract class Bakery {
    public abstract Pizza prepare(PizzaModel model);
    public abstract void bake(Pizza pizza);
    public abstract void cut(Pizza pizza);
    public abstract void box(Pizza pizza);
}

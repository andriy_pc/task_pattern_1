package units.pizza;

public enum Toppings {
    SAUSAGE("Meat sausage"),
    CHEESE("Mozzarella cheese"),
    TOMATOES("Fresh tomatoes"),
    OLIVES("Italian olives"),
    PINEAPPLE("Pickled pineapple");
    private String description;
    Toppings(String desc) {
        description = desc;
    }
}

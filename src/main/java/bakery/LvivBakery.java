package bakery;

import units.pizza.*;

public class LvivBakery extends Bakery {

    @Override
    public Pizza prepare(PizzaModel model) {
        Pizza pizza = new Pizza();
        if(model == PizzaModel.CHEESE) {
            pizza.setDough(Dough.CRUST);
            pizza.setSauce(Sauce.MARINARA);
            pizza.setToppings(Toppings.CHEESE, Toppings.CHEESE);
        } else if(model == PizzaModel.VEGGIE) {
            /*Some code here*/
        }
        return pizza;
    }

    @Override
    public void bake(Pizza pizza) {
        pizza.setBaked(true);
    }

    @Override
    public void cut(Pizza pizza) {
        pizza.setBoxed(true);
    }

    @Override
    public void box(Pizza pizza) {
        pizza.setBoxed(true);
    }
}

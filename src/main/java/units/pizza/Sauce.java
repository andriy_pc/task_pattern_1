package units.pizza;

public enum Sauce {
    MARINARA("Marinara sauce"),
    PLUM_TOMATO("Plum tomato sauce"),
    PESTO("Pesto sauce");
    private String description;
    Sauce(String desc) {
        description = desc;
    }
}

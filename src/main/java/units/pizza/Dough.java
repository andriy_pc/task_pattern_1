package units.pizza;

public enum Dough {
    THICK("Thick dough"),
    THIN("Thin dough"),
    CRUST("Crust dough");
    private String description;
    Dough(String desc) {
        this.description = desc;
    }
}

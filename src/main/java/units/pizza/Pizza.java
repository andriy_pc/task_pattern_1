package units.pizza;

public class Pizza {

    private Dough dough;
    private Sauce sauce;
    private Toppings[] toppings;
    private boolean baked;
    private boolean cut;
    private boolean boxed;

    public void setBaked(boolean baked) {
        this.baked = baked;
    }

    public void setCut(boolean cut) {
        this.cut = cut;
    }

    public void setBoxed(boolean boxed) {
        this.boxed = boxed;
    }

    public Dough getDough() {
        return dough;
    }

    public void setDough(Dough dough) {
        this.dough = dough;
    }

    public Sauce getSauce() {
        return sauce;
    }

    public void setSauce(Sauce sauce) {
        this.sauce = sauce;
    }

    public Toppings[] getToppings() {
        return toppings;
    }

    public void setToppings(Toppings topping1, Toppings topping2) {
        Toppings[] result = new Toppings[] {topping1, topping2};
        this.toppings = result;
    }
}
